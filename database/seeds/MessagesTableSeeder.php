<?php

/** php artisan make:seed MessagesTableSeeder */

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * No model available so run with db insert
         *
         */

        // TODO: get first admin user and assign
        DB::table('messages')->insert([
            'body' => 'First message',
            'user_id' => 1,
            'created_at' => Carbon::now(),
        ]);

        // TODO: get first non-admin user and assign
        DB::table('messages')->insert([
            'body' => 'second message',
            'user_id' => 1,
            'created_at' => Carbon::now(),
        ]);
    }
}
