<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * User model is available so use it to insert into db
     * @return void
     */
    public function run()
    {
        // Add admin user
        $role_admin = Role::where('name', 'admin')->first();
        $user_admin = User::create([
            'name' => 'Brendon',
            'email' => 'brpots07@gmail.com',
            'password' => Hash::make('Aj7iF0!VG^5y'),
            'created_at' => Carbon::now(),
        ]);
        $user_admin->roles()->attach($role_admin);

        // Add normal user
        $role_user = Role::where('name', 'user')->first();
        $user_user = User::create([
            'name' => 'Visitor',
            'email' => 'brpots07+1@gmail.com',
            'password' => Hash::make('Aj7iF0!VG^5y'),
            'created_at' => Carbon::now(),
        ]);
        $user_user->roles()->attach($role_user);

    }
}
