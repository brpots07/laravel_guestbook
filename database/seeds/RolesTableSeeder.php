<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'admin',
            'description' => 'This role has no limitations'
        ]);

        Role::create([
            'name' => 'user',
            'description' => 'Only CRUD on own messages'
        ]);
    }
}
