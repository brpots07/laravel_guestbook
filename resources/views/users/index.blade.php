@extends('layouts.app')

@section('title', 'Guestbook messages')

@section('content')
<body>
    <ul>
        @foreach ($users as $user)

            <li>{{ $user }}</li>

        @endforeach
    </ul>
</body>

@endsection
