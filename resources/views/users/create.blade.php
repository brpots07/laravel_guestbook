@extends('layouts.app')

@section('title', 'Leave a message')

@section('content')
<body>
    <!-- Comments Form -->
    <div class="card my-4">
        <h5 class="card-header">Leave a Message:</h5>
        <div class="card-body">
        {{-- <form method="post" actions="{{ route('messages.store') }}" > --}}
        <form method="post" actions="/messages" >
            @csrf
            <div class="form-group">
            <textarea class="form-control" ></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
    </div>
</body>

@endsection
