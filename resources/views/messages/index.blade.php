@extends('layouts.app')

@section('content')
<div class="container">

    @foreach ($messages as $message)
    @include('messages.message')
    <hr>
    @endforeach
</div>

@endsection
