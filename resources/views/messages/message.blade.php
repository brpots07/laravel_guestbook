
<div class="col-lg-8">
    <!-- Message Body -->
    <p>{{ $message->body }}</p>

    <!-- Author -->
    <footer class="blockquote-footer">
        by {{ $message->user->name }} on {{ $message->created_at->toFormattedDateString() }}

    </footer>
    @auth
        @if($message->isAllowed())
            <a href="{{ route('messages.show', $message->id) }}">message actions</a>
        @endif
        <br>
    @endauth


    <!-- message comments -->
    @if(count($message->comments))
        @include('comments.show')
    @endif

</div>
