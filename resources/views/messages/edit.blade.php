@extends('layouts.app')

@section('title', '| Edit message')

@section('content')
<div class="row">

    <div class="col-md-8 col-md-offset-2">

        <h1>Edit message</h1>
        <hr>
        @include ('errors.list')
            {{ Form::model($message, array('route' => array('messages.update', $message->id), 'method' => 'PUT')) }}
            <div class="form-group">

                {{ Form::label('body', 'message Body') }}
                {{ Form::textarea('body', null, array('class' => 'form-control')) }}<br>

                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>
    </div>
</div>

@endsection
