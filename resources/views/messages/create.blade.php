@extends('layouts.app')

@section('title', '| Post new message')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <h1>Add New Message</h1>
            <hr>

            {{-- Using the Laravel HTML Form Collective to create our form --}}
            {{ Form::open(array('route' => 'messages.store')) }}

            <div class="form-group">

                {{ Form::label('body', 'Message Body') }}
                {{ Form::textarea('body', null, array('class' => 'form-control')) }}
                <br>

                {{ Form::submit('Post Message', array('class' => 'btn btn-success btn-lg btn-block')) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection
