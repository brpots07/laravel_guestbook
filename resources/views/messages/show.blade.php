@extends('layouts.app')

@section('title', '| View message')

@section('content')

<div class="container">

    <hr>
    <p class="lead">{{ $message->body }} </p>
    <hr>
    <!-- message comments -->
    @if(count($message->comments))
        @include('comments.show')
    @endif

    @if(Auth::user()->isAdmin())
        @include('comments.create')
    @endif

    {!! Form::open(['method' => 'DELETE', 'route' => ['messages.destroy', $message->id] ]) !!}
        <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>

        @if($message->isAllowed())
            <a href="{{ route('messages.edit', $message->id) }}" class="btn btn-info" role="button">Edit</a>
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        @endif
    {!! Form::close() !!}

</div>

@endsection
