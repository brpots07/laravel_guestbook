<br>
<form method="POST" action="{{ route('comment.add', $message->id) }}">
    @csrf
    <div class="form-group">
        <input type="text" name="body" class="form-control" />
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-warning" value="Add Comment" />
    </div>
</form>
