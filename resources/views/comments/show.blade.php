
<br>
<h6>Message comments:</h6>
@foreach ($message->comments as $comment)

    <!-- nested comments -->
    <p>{{ $comment->body }}</p>
    <!-- Author -->
    <footer class="blockquote-footer">
        by {{ $comment->user->name }} on {{ $comment->created_at->toFormattedDateString()}}
    </footer>
        <br>
@endforeach

