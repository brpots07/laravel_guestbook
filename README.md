# laravel_guestbook

This project is a laravel training project. The goal is to create a guestbook application.

## Project requirements
Create a guestbook using the Laravel framework and a database of your choice. 
The requirements are open-ended, so use your own initiative on missing parts. 

The guestbook should allow for the following:


1. Register of a user
1. Login of a user
1. User levels (admin)
1. CRUD operations by a registered user on their own messages
1. Replying to messages sent by user (admin)
1. Deleting any user's messages (admin) 

The visual aspect of this task (HTML) will not be highly evaluated. Unit tests are not required but would be an advantage.


## Tech/frameworks
*  Laravel 

## Server Requirements
Please see the [Laravel Installation](https://laravel.com/docs/6.x) guide for what is required to run Laravel. This project uses [Laravel Homestead](https://laravel.com/docs/6.x/homestead) which will satisfy all the requirements and have a running version of the application.

### Laravel homesteaad requirements
* [Virtual machine](https://laravel.com/docs/6.x/homestead#first-steps)
* [Vagrant](https://www.vagrantup.com/)


## Installation
* TODO:
* [Homestead setup](https://github.com/gitname/laravel-homestead-tutorial/blob/master/README.md)


## Usage
TODO:

## Improvements
Implement can method with policies
