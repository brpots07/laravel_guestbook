<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'body', 'user_id', 'message_id'
    ];

    public function message()
    {
        return $this->belongs(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
