<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // public function roles()
    // {
    //     return $this->belongsToMany(Role::class);
    // }

    public function hasRole()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Check if a user is an admin
     */
    public function isAdmin()
    {
        return $this->hasRole()->where('name', 'admin')->exists();
    }


    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * Check if user created the message
     */
    // public function isAllowed()
    // {
    //     Log::info($this->user);
    //     // return $this->messages()->user_id;
    // }

    public function comments()
    {
        return $this->belongsToMany(Comment::class);
    }

    public function publish(Message $message)
    {
        // Log::info('publish@UserModel');
        // Log::info($message);
        $this->messages()->save($message);
    }
}
