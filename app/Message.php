<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body', 'user_id'
    ];


    // protected $guarded = [
    //     'user_id'
    // ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if message can be editted
     * admin or message creator
     */
    public function isAllowed()
    {
        // Log::info('isAllowed@MessageModel');
        // Log::info(Auth::user());
        // Log::info($this->user_id);
        return $this->user_id === Auth::user()->id || Auth::user()->isAdmin();

    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function addComment($body)
    {
        $this->comments()->create(['body' => $body,
                                'user_id' => Auth::user()->id]);
    }

    public function removeComment()
    {
        // Log::info('MessageModel@removeComment');
        // Log::info($this->comments);
        $this->comments->each->delete();

    }


}
