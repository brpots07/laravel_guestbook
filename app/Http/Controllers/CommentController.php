<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Message;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

class CommentController extends Controller
{

    public function __construct()
    {
        // TODO: This should be admin users only toggle in view
        $this->middleware(['auth']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Message $message)
    {
        // Log::info('MessagesController@store');
        // Log::info($message);
        // Log::info(request('body'));

        $this->validate(request(), ['body' => 'required|min:2']);

        $message->addComment(request('body'));

        return back();
    }

}
