<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Message;

use Illuminate\Support\Facades\Log;



class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth'])->except('index');
        $this->middleware(['isAllowed'])->except(['index', 'store', 'create']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * todo: no auth to see messages
     */
    public function index()
    {
        $messages = Message::all()->sortByDesc("id");       // TODO: sort by updated date then id

        return view('messages.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'body' => 'required'
        ]);

        auth()->user()->publish(
            new Message(request(['body']))
        );

        return redirect('/messages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * Route model binding here instead of passing id
     * to work variable name = route wildcard
     * $message => {message} laravel assumes primary key. This can be updated

     */
    public function show(Message $message)
    {
        return view('messages.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        return view('messages.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Message $message)
    {
        $this->validate(request(), [
            'body' => 'required'
        ]);

        $message->body = request('body');
        $message->save();

        return redirect('/messages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        $message->removeComment();
        $message->delete();
        return redirect('/messages');
    }
}
