<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class isAllowed
{
    /**
     * Check if a user has access to a messages actions
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!$request->message->isAllowed()){
            // TODO flash message no access
            return redirect('/messages');
        }

        return $next($request);
    }
}
