<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();             // src/Illuminate/Support/Facades/Auth.php
Route::get('/', 'MessageController@index');


// Message routes
Route::resource('messages', 'MessageController');

Route::resource('users', 'UserController');

Route::post('/messages/{message}/comments', 'CommentController@store')->name('comment.add');


